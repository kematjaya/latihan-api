<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter as Filters;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *  itemOperations={"get","delete","put"}
 * )
 * @ApiFilter(Filters\DateFilter::class, properties={"created_at", "tanggal_berakhir"})
 * @ApiFilter(Filters\SearchFilter::class, properties={"type":"exact", "nomor":"partial"})
 * @ORM\Table(name="t_perusahaan_ijin_usaha")
 * @ORM\Entity(repositoryClass="App\Repository\TPerusahaanIjinUsahaRepository")
 */
class TPerusahaanIjinUsaha
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomor;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MPerusahaan", inversedBy="tPerusahaanIjinUsahas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $perusahaan;

    /**
     * @ORM\Column(type="date")
     */
    private $tanggal_terbit;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $tanggal_berakhir;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNomor(): ?string
    {
        return $this->nomor;
    }

    public function setNomor(string $nomor): self
    {
        $this->nomor = $nomor;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getPerusahaan(): ?MPerusahaan
    {
        return $this->perusahaan;
    }

    public function setPerusahaan(?MPerusahaan $perusahaan): self
    {
        $this->perusahaan = $perusahaan;

        return $this;
    }

    public function getTanggalTerbit(): ?\DateTimeInterface
    {
        return $this->tanggal_terbit;
    }

    public function setTanggalTerbit(\DateTimeInterface $tanggal_terbit): self
    {
        $this->tanggal_terbit = $tanggal_terbit;

        return $this;
    }

    public function getTanggalBerakhir(): ?\DateTimeInterface
    {
        return $this->tanggal_berakhir;
    }

    public function setTanggalBerakhir(?\DateTimeInterface $tanggal_berakhir): self
    {
        $this->tanggal_berakhir = $tanggal_berakhir;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
