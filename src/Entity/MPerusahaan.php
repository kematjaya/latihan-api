<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter as Filters;
use App\Validator\Constraints\UniqueField;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *  itemOperations={"get","delete","put"}
 * )
 * @ApiFilter(Filters\OrderFilter::class, properties={"id", "npwp", "nama", "email"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(Filters\SearchFilter::class, properties={"npwp":"partial", "nama":"partial", "email":"partial"})
 * @ORM\Table(name="m_perusahaan")
 * @ORM\Entity(repositoryClass="App\Repository\MPerusahaanRepository")
 */
class MPerusahaan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $npwp;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $nama;

    /**
     * @UniqueField
     * @Assert\NotBlank
     * @Assert\Email(message="The email '{{ value }}' is not a valid email.")
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $alamat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telp;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TPerusahaanIjinUsaha", mappedBy="perusahaan", orphanRemoval=true)
     */
    private $tPerusahaanIjinUsahas;

    public function __construct()
    {
        $this->tPerusahaanIjinUsahas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNpwp(): ?string
    {
        return $this->npwp;
    }

    public function setNpwp(string $npwp): self
    {
        $this->npwp = $npwp;

        return $this;
    }

    public function getNama(): ?string
    {
        return $this->nama;
    }

    public function setNama(string $nama): self
    {
        $this->nama = $nama;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAlamat(): ?string
    {
        return $this->alamat;
    }

    public function setAlamat(?string $alamat): self
    {
        $this->alamat = $alamat;

        return $this;
    }

    public function getTelp(): ?string
    {
        return $this->telp;
    }

    public function setTelp(?string $telp): self
    {
        $this->telp = $telp;

        return $this;
    }

    /**
     * @return Collection|TPerusahaanIjinUsaha[]
     */
    public function getTPerusahaanIjinUsahas(): Collection
    {
        return $this->tPerusahaanIjinUsahas;
    }

    public function addTPerusahaanIjinUsaha(TPerusahaanIjinUsaha $tPerusahaanIjinUsaha): self
    {
        if (!$this->tPerusahaanIjinUsahas->contains($tPerusahaanIjinUsaha)) {
            $this->tPerusahaanIjinUsahas[] = $tPerusahaanIjinUsaha;
            $tPerusahaanIjinUsaha->setPerusahaan($this);
        }

        return $this;
    }

    public function removeTPerusahaanIjinUsaha(TPerusahaanIjinUsaha $tPerusahaanIjinUsaha): self
    {
        if ($this->tPerusahaanIjinUsahas->contains($tPerusahaanIjinUsaha)) {
            $this->tPerusahaanIjinUsahas->removeElement($tPerusahaanIjinUsaha);
            // set the owning side to null (unless already changed)
            if ($tPerusahaanIjinUsaha->getPerusahaan() === $this) {
                $tPerusahaanIjinUsaha->setPerusahaan(null);
            }
        }

        return $this;
    }
}
