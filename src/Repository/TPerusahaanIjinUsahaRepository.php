<?php

namespace App\Repository;

use App\Entity\TPerusahaanIjinUsaha;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TPerusahaanIjinUsaha|null find($id, $lockMode = null, $lockVersion = null)
 * @method TPerusahaanIjinUsaha|null findOneBy(array $criteria, array $orderBy = null)
 * @method TPerusahaanIjinUsaha[]    findAll()
 * @method TPerusahaanIjinUsaha[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TPerusahaanIjinUsahaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TPerusahaanIjinUsaha::class);
    }

    // /**
    //  * @return TPerusahaanIjinUsaha[] Returns an array of TPerusahaanIjinUsaha objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TPerusahaanIjinUsaha
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
