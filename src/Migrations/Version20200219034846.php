<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219034846 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE mperusahaan_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE m_perusahaan_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE m_perusahaan (id INT NOT NULL, npwp VARCHAR(255) NOT NULL, nama VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, alamat TEXT DEFAULT NULL, telp VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE mperusahaan');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE m_perusahaan_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE mperusahaan_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE mperusahaan (id INT NOT NULL, npwp VARCHAR(255) NOT NULL, nama VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, alamat TEXT DEFAULT NULL, telp VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE m_perusahaan');
    }
}
