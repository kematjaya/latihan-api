<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200219041808 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE t_perusahaan_ijin_usaha_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE t_perusahaan_ijin_usaha (id INT NOT NULL, perusahaan_id INT NOT NULL, type VARCHAR(255) NOT NULL, nomor VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, tanggal_terbit DATE NOT NULL, tanggal_berakhir DATE DEFAULT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_987DF79EFB6AC757 ON t_perusahaan_ijin_usaha (perusahaan_id)');
        $this->addSql('ALTER TABLE t_perusahaan_ijin_usaha ADD CONSTRAINT FK_987DF79EFB6AC757 FOREIGN KEY (perusahaan_id) REFERENCES m_perusahaan (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE t_perusahaan_ijin_usaha_id_seq CASCADE');
        $this->addSql('DROP TABLE t_perusahaan_ijin_usaha');
    }
}
