<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class UniqueField extends Constraint {
    public $message = 'The string "{{ string }}" is already exist.';
}
