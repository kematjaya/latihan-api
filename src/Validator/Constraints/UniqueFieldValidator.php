<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class UniqueFieldValidator extends ConstraintValidator {
    
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function validate($value, Constraint $constraint) {
        $object = $this->context->getObject();
        $obj = [];
        if(!$object->getId()) 
        {
            $obj = $this->entityManager->getRepository($this->context->getClassName())->findBy([$this->context->getPropertyPath() => $value]);
        } 
        else 
        {
            $qb = $this->entityManager->createQueryBuilder()
                    ->addSelect('t.id')->addSelect('t.'.$this->context->getPropertyPath())
                    ->from($this->context->getClassName(), 't')
                    ->where('t.id != :id')
                    ->andWhere('t.'.$this->context->getPropertyPath() . ' = :'.$this->context->getPropertyPath())
                    ->setParameter('id', $object->getId())
                    ->setParameter($this->context->getPropertyPath(), $value);
            $obj = $qb->getQuery()->getResult();
        }
        if(!empty($obj))
        {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }

}
